= What's New in Antora
:route: New
:doctype: book
:url-releases-asciidoctor: https://github.com/asciidoctor/asciidoctor/releases
:url-releases-asciidoctorjs: https://github.com/asciidoctor/asciidoctor.js/releases
:url-gitlab: https://gitlab.com
:url-git-antora: {url-gitlab}/antora/antora
:url-issues: {url-git-antora}/issues
:url-milestone-2-1-0: {url-issues}?scope=all&state=closed&label_name%5B%5D=%5BVersion%5D%202.1.0
:url-milestone-2-1-1: {url-issues}?scope=all&state=closed&label_name%5B%5D=%5BVersion%5D%202.1.1
:url-mr: {url-git-antora}/merge_requests

= Antora 2.1.1

_**Release date:** 2019.09.09 | *Issue label:* {url-milestone-2-1-1}[2.1.1^]_

The Antora 2.1.1 patch release makes the site URL more flexible (and validates it), corrects the timestamp of files in the published archive, and fixes an inconsistency in the playbook schema.

== A more flexible site URL

When the site URL is set in the playbook using the `site.url` key or `--url` CLI option, it activates several additional features.
Those features include the sitemap, the canonical URLs, and the 404 page.
The site URL is also used when producing redirects.
However, not all these features require an absolute URL.
Some only require the pathname segment of the URL (the part after the domain name).

Therefore, Antora now allows you to set the site URL to a pathname.
In that case, any feature that requires an absolute URL will remain disabled.
Those features are the sitemap and the canonical URLs (as required by the powers that be).
The 404 page will still be created (since it doesn't need an absolute URL), and the pathname will also be used as the prefix for any paths in the redirect rules.

TIP: If you want the pathname to be empty, set the value to `/`.

To summarize, the site URL can be either an absolute URL (with an optional pathname) or just a pathname.
The playbook builder now validates that the site URL conforms to one of these two types.

== Ensure .git suffix got camelCased

The new key to control whether the git client automatically appends the .git suffix to the content source URL if missing was defined using camelCase, as in `git.ensureGitSuffix`.
This naming is inconsistent with all other keys in the playbook schema.
It should have been named `git.ensure_git_suffix`.
As of this patch release, it now is.
In other words:

[source,yml]
----
git:
  ensure_git_suffix: false
----

The incorrect name is still supported, but should be considered deprecated.

== Timestamps in the published archive now live in the present

When Antora reads files from a git tree, it does not assignment a timestamp to the file, meaning the mtime is undefined.
That way, when the file is written, it gets assigned the current timestamp.
At least, that's how it's supposed to work.

What the code was actually doing was assigning an invalid date.
While this wasn't a problem for the filesystem provider, it was for the archive provider.
So all the files in the archive (ZIP) file that came from the git tree (and supplemental UI files that came from the playbook) had a timestamp of the Unix epoch (shortly after Woodstock).
That caused certain cloud deployers to get rather confused (and dazed?).
This patch release corrects this problem so that the timestamp of the files in the archive match the timestamp of the archive itself.

== Resolved Issues

Issue {url-issues}/478[#478^]:: Allow site.url to be a pathname, but disable any features that require an absolute URL.
Issue {url-issues}/471[#471^]:: Files in a generated ZIP site bundle have a timestamp of the Unix epoch.
Issue {url-issues}/479[#479^]:: Playbook builder should validate site.url.
Issue {url-issues}/477[#477^]:: Name of playbook key for controlling git suffix (ensureGitSuffix) is inconsistent.

= Antora 2.1.0

_**Release date:** 2019.08.27 | *Issue label:* {url-milestone-2-1-0}[2.1.0^]_

The Antora 2.1.0 release provides more image capabilities, adds a new page attribute and playbook key, and improves path handling for 404 pages and rewrite rules.

== Reference images from other modules, components, and versions using Antora's resource ID

Antora 1.1 introduced the resource ID for referencing xref:asciidoc:include-partial.adoc[partial] and xref:asciidoc:include-example.adoc[example files] stored in other modules, components, and versions.
Now you can use the resource ID to reference images in the AsciiDoc image macro, too!

```
image::module-b:image.png[]

image::2.0@image.png[]
```

See xref:asciidoc:insert-image.adoc[Insert an Image] for more image macro and resource ID examples.
See <<deprecation>> for future image referencing changes.

== The assets folder is now optional

You no longer have to store your images and attachments inside the [.path]_assets_ folder.
These folders can be top-level folders inside a module.

....
modules/
  ROOT/
    attachments/
    examples/
    images/
    pages/
    partials/
....

And don't worry if you prefer using the assets folder or currently have files in an assets folder, Antora still collects images and attachments stored in the assets folder.
See xref:component-structure.adoc[Documentation Components and Repositories] and xref:modules.adoc[Modules, Families, and Subdirectories] to learn more about how to organize your documentation source files.

== More new features and improvements

Control the git repository URL suffix with ensureGitSuffix:: The new playbook key `ensureGitSuffix` instructs the git client to automatically append .git to the repository URL if it is absent.
You don't have to add this key to your Antora playbook if you're using GitHub or GitLab because Antora automatically sets `ensureGitSuffix` to `true` by default.
However, if you use Team Foundation Server (TFS) or Azure DevOps, set `ensureGitSuffix: false` under the `git` category of your playbook if your content source repositories fail to clone.

Access implicit page attributes from navigation files:: Antora's implicit xref:page:page-and-site-attributes.adoc#page-attributes[page attributes] can now be used in AsciiDoc navigation files.

Unresolved cross references and their link text:: The link text of an unresolved page reference is now displayed with the page ID when an xref can't be resolved.

Access a page's display version:: The component display version is now accessible via the implicit page attribute `page-component-display-version`.
This attribute contains the display version value found in the component descriptor ([.path]_antora.yml_).

404 pages and site paths:: Antora automatically prepends the site path (pathname of site URL) to `siteRootPath` and `uiRootPath` on the 404 page.

== Fixes

Issue {url-issues}/258[#258^]:: The pathname of the site URL (site path) is now assigned to the `site.path` property in the UI model.
An empty string is used if the site URL is not set or the pathname is `/`.
To get the full fix, you must update your UI.

Issue {url-issues}/468[#468^]:: A site URL's trailing slash is now removed before it's used to compute rewrite rules.

[#deprecation]
== Scheduled deprecations for Antora 3.0

The ability to use parent references in the target of the AsciiDoc image macro (e.g., `image::../../../module-b/_images/image-filename.png[]`) will be deprecated in Antora 3.0.
You should begin replacing any such image targets with resource IDs.

== Antora 1.1 EOL

Antora 1.1 reaches its end of life on August 31, 2019.
If you're using Antora 1.1, review the xref:2.0@whats-new.adoc#antora-2-0-0[Antora 2.0 release notes] and xref:2.0@whats-new.adoc#checklist[upgrade checklist] before upgrading to the latest Antora release.
